
function varargout = Giaodien(varargin)
% GIAODIEN MATLAB code for Giaodien.fig
%      GIAODIEN, by itself, creates a new GIAODIEN or raises the existing
%      singleton*.
%
%      H = GIAODIEN returns the handle to a new GIAODIEN or the handle to
%      the existing singleton*.
%
%      GIAODIEN('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GIAODIEN.M with the given input arguments.
%
%      GIAODIEN('Property','Value',...) creates a new GIAODIEN or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Giaodien_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Giaodien_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Giaodien

% Last Modified by GUIDE v2.5 12-May-2019 15:41:22

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Giaodien_OpeningFcn, ...
                   'gui_OutputFcn',  @Giaodien_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Giaodien is made visible.
function Giaodien_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Giaodien (see VARARGIN)

% Choose default command line output for Giaodien
handles.output = hObject;
Node = [];
Threshold_Weight = double(0);
RPARM = double(0);
Capacity = double(0);
ipt1 = string(0);
ipt2 = string(0);
ipt3 = string(0);
ipt4 = string(0);
ipt5 =string(0);
ipt6 = string(0);
ipt7 = string(0);
ipt8 = string(0);
ipt9 = string(0);
ipt10 = string(0);
ipt11 = string(0);
x_Node = [];
y_Node = [];
Node_Weight = [];
Backbone_Node=[];
Center_Backbone_Node=[];
Traffic = [];
Traffic_1 = [];
home = [];
handles.Traffic=Traffic;
handles.Traffic_1 = Traffic_1;
handles.home = home;
handles.Backbone_Node=Backbone_Node;
handles.Center_Backbone_Node=Center_Backbone_Node;
handles.Node_Weight = Node_Weight;
handles.x_Node = x_Node;
handles.y_Node = y_Node;
handles.ipt1 = ipt1;
handles.ipt2 = ipt2;
handles.ipt3 = ipt3;
handles.ipt4 = ipt4;
handles.ipt5 = ipt5;
handles.ipt6 = ipt6;
handles.ipt7 = ipt7;
handles.ipt8 = ipt8;
handles.ipt9 = ipt9;
handles.ipt10 = ipt10;
handles.ipt11 = ipt11;
handles.Node = Node;
handles.Threshold_Weight = Threshold_Weight;
handles.RPARM = RPARM;
handles.Capacity = Capacity;
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Giaodien wait for user response (see UIRESUME)
% uiwait(handles.figure1);

% --- Outputs from this function are returned to the command line.
function varargout = Giaodien_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function input3_Callback(hObject, eventdata, handles)
% hObject    handle to input3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of input3 as text
%        str2double(get(hObject,'String')) returns contents of input3 as a double


% --- Executes during object creation, after setting all properties.
function input3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to input3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function input5_Callback(hObject, eventdata, handles)
% hObject    handle to input5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of input5 as text
%        str2double(get(hObject,'String')) returns contents of input5 as a double



% --- Executes during object creation, after setting all properties.
function input5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to input5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function input1_Callback(hObject, eventdata, handles)
% hObject    handle to input1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of input1 as text
%        str2double(get(hObject,'String')) returns contents of input1 as a double


% --- Executes during object creation, after setting all properties.
function input1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to input1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function input6_Callback(hObject, eventdata, handles)
% hObject    handle to input6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of input6 as text
%        str2double(get(hObject,'String')) returns contents of input6 as a double


% --- Executes during object creation, after setting all properties.
function input6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to input6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function input2_Callback(hObject, eventdata, handles)
% hObject    handle to input2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of input2 as text
%        str2double(get(hObject,'String')) returns contents of input2 as a double


% --- Executes during object creation, after setting all properties.
function input2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to input2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function input7_Callback(hObject, eventdata, handles)
% hObject    handle to input7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of input7 as text
%        str2double(get(hObject,'String')) returns contents of input7 as a double


% --- Executes during object creation, after setting all properties.
function input7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to input7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function input4_Callback(hObject, eventdata, handles)
% hObject    handle to input4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of input4 as text
%        str2double(get(hObject,'String')) returns contents of input4 as a double


% --- Executes during object creation, after setting all properties.
function input4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to input4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function input8_Callback(hObject, eventdata, handles)
% hObject    handle to input8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of input8 as text
%        str2double(get(hObject,'String')) returns contents of input8 as a double


% --- Executes during object creation, after setting all properties.
function input8_CreateFcn(hObject, eventdata, handles)
% hObject    handle to input8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function input9_Callback(hObject, eventdata, handles)
% hObject    handle to input9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of input9 as text
%        str2double(get(hObject,'String')) returns contents of input9 as a double


% --- Executes during object creation, after setting all properties.
function input9_CreateFcn(hObject, eventdata, handles)
% hObject    handle to input9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function input10_Callback(hObject, eventdata, handles)
% hObject    handle to input10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of input10 as text
%        str2double(get(hObject,'String')) returns contents of input10 as a double


% --- Executes during object creation, after setting all properties.
function input10_CreateFcn(hObject, eventdata, handles)
% hObject    handle to input10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function input11_Callback(hObject, eventdata, handles)
% hObject    handle to input11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of input11 as text
%        str2double(get(hObject,'String')) returns contents of input11 as a double


% --- Executes during object creation, after setting all properties.
function input11_CreateFcn(hObject, eventdata, handles)
% hObject    handle to input11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
handles.ipt1 = get(handles.input1,'String');
handles.ipt2 = get(handles.input2,'String');
handles.ipt3 = get(handles.input3,'String');
handles.ipt4 = get(handles.input4,'String');
handles.ipt5 = get(handles.input5,'String');
handles.ipt6 = get(handles.input6,'String');
handles.ipt7 = get(handles.input7,'String');
handles.ipt8 = get(handles.input8,'String');
handles.ipt9 = get(handles.input9,'String');
handles.ipt10 = get(handles.input10,'String');
handles.ipt11 = get(handles.input11,'String');
%Khoi tao cac nut ngau nhien
rng('shuffle','v5normal');
handles.Node=randi(str2double(handles.ipt1),str2double(handles.ipt3),2); %Tao mot ma tran co kich thuoc [So nut x 2]
handles.x_Node=handles.Node(:,1);   %Lay cot dau tien la gia tri x
handles.y_Node=handles.Node(:,2);   %Lay cot dau tien la gia tri y
handles.Traffic=zeros(length(handles.Node));
for i=1:length(handles.Node)-3
    handles.Traffic(i,i+3)=1;
    handles.Traffic(i+3,i)=1;
end
for i=1:length(handles.Node)-6
    handles.Traffic(i,i+6)=2;
    handles.Traffic(i+6,i)=2;
end
for i=1:length(handles.Node)-7
    handles.Traffic(i,i+7)=3;
    handles.Traffic(i+7,i)=3;
end
assignin('base','Traffic_2_before',handles.Traffic);
axis([0 str2double(handles.ipt1) 0 str2double(handles.ipt2)]);  %Gioi han truc X, Y
axis square;
xlabel('x');ylabel('y');
hold on;
for i=1:length(handles.Node)
    draw(i)=scatter(handles.x_Node(i),handles.y_Node(i),'ok');  %Ve cac nut ngau nhien
    Show_Index(i)=text(handles.x_Node(i)-10,handles.y_Node(i)-20,num2str(i));   %Danh so thu tu cho cac nut
end
% waitforbuttonpress;     %Click chuot hoac nhan nut bat ky de tiep tuc
%Khoi tao trong so (Weight) cua cac nut
handles.Node_Weight=ones(1,length(handles.Node));
handles.Node_Weight(5)=10;handles.Node_Weight(60)=10;handles.Node_Weight(70)=10;
handles.Node_Weight(7)=15;handles.Node_Weight(23)=15;handles.Node_Weight(45)=15;
handles.Node_Weight(52)=5;handles.Node_Weight(65)=5;handles.Node_Weight(78)=5;
handles.Node_Weight(32)=3;handles.Node_Weight(57)=3;handles.Node_Weight(48)=3;

%Khoi tao trong so nguong (Threshold Weight), handles.RPARM, dung luong (handles.Capacity)
handles.Threshold_Weight=str2double(handles.ipt4); 
handles.RPARM=str2double(handles.ipt5);
handles.Capacity=str2double(handles.ipt6); 
guidata(hObject, handles);

% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton6.
function pushbutton6_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%Tinh Cost
Cost=zeros(length(handles.Node));
for i=1:length(handles.Node)
    for j=1:length(handles.Node)
        Cost(j,i)=0.4*sqrt((handles.x_Node(j)-handles.x_Node(i))^2+(handles.y_Node(j)-handles.y_Node(i))^2);
    end
end
%Tim cac nut backbone thoa man tieu chuan trong so
Normalized_Weight=zeros(1,length(handles.Node));    %Khoi tao trong so chuan hoa (Normalized Weight)
for i=1:length(handles.Node)
    Normalized_Weight(i)=handles.Node_Weight(i)/handles.Capacity;
    if Normalized_Weight(i)> handles.Threshold_Weight
        handles.Backbone_Node=[handles.Backbone_Node i];
    end
end
delete(plot(handles.Backbone_Node));    %Chuan bi ve nut Backbone
for i=1:numel(handles.Backbone_Node)
    draw(handles.Backbone_Node(i))=scatter(handles.x_Node(handles.Backbone_Node(i)),handles.y_Node(handles.Backbone_Node(i)),'r','filled');
end
%Tim cac nut truy nhap cho nut cac backbone
Max_Cost=max(max(Cost));    %Tinh Max Costa
Radius=Max_Cost*handles.RPARM;     %Tinh ban kinh duong tron
access_node_for_1st_backbone=[];    %Khoi tao cac nut truy nhap cho nut Backbone thu nhat
Condition1=handles.Backbone_Node;   %Dieu kien xet
for i=1:length(handles.Node)
    if (i~=Condition1)&(0.4*(sqrt((handles.x_Node(i)-handles.x_Node(handles.Backbone_Node(1)))^2+(handles.y_Node(i)-handles.y_Node(handles.Backbone_Node(1)))^2))<=Radius)
        access_node_for_1st_backbone=[access_node_for_1st_backbone i];  %Them nut i vao tap hop cac nut truy nhap
        scatter(handles.x_Node(access_node_for_1st_backbone),handles.y_Node(access_node_for_1st_backbone),'ok');
        node_index(i)= handles.Backbone_Node(1);     %Ghi lai xem nut truy nhap thuoc backbone nao
    end
end
for i=1:numel(access_node_for_1st_backbone)
    plot([handles.x_Node(handles.Backbone_Node(1)),handles.x_Node(access_node_for_1st_backbone(i))],[handles.y_Node(handles.Backbone_Node(1)),handles.y_Node(access_node_for_1st_backbone(i))],'g');
end
access_node_for_2nd_backbone=[];
Condition1=[Condition1 access_node_for_1st_backbone];
for i=1:length(handles.Node)
    if (i~=Condition1)&(0.4*(sqrt((handles.x_Node(i)-handles.x_Node(handles.Backbone_Node(2)))^2+(handles.y_Node(i)-handles.y_Node(handles.Backbone_Node(2)))^2))<=Radius)
        access_node_for_2nd_backbone=[access_node_for_2nd_backbone i];
        scatter(handles.x_Node(access_node_for_2nd_backbone),handles.y_Node(access_node_for_2nd_backbone),'ok');
        node_index(i)=handles.Backbone_Node(2);
    end
end
for i=1:numel(access_node_for_2nd_backbone)
    plot([handles.x_Node(handles.Backbone_Node(2)),handles.x_Node(access_node_for_2nd_backbone(i))],[handles.y_Node(handles.Backbone_Node(2)),handles.y_Node(access_node_for_2nd_backbone(i))],'g');
end
access_node_for_3rd_backbone=[];
Condition1=[Condition1 access_node_for_2nd_backbone];
for i=1:length(handles.Node)
    if (i~=Condition1)&(0.4*(sqrt((handles.x_Node(i)-handles.x_Node(handles.Backbone_Node(3)))^2+(handles.y_Node(i)-handles.y_Node(handles.Backbone_Node(3)))^2))<=Radius)
        access_node_for_3rd_backbone=[access_node_for_3rd_backbone i];
        scatter(handles.x_Node(access_node_for_3rd_backbone),handles.y_Node(access_node_for_3rd_backbone),'ok');
        node_index(i)=handles.Backbone_Node(3);
    end
end
for i=1:numel(access_node_for_3rd_backbone)
    plot([handles.x_Node(handles.Backbone_Node(3)),handles.x_Node(access_node_for_3rd_backbone(i))],[handles.y_Node(handles.Backbone_Node(3)),handles.y_Node(access_node_for_3rd_backbone(i))],'g');
end
access_node_for_4th_backbone=[];
Condition1=[Condition1 access_node_for_3rd_backbone];
%--------------------------------------------------------------------------
%Tim cac nut backbone con lai
%Xac dinh Center of Gravity - CG(trung tam cua trong luc)
x_center_of_gravity=sum(handles.Node_Weight(:).*handles.x_Node(:))/sum(handles.Node_Weight);
y_center_of_gravity=sum(handles.Node_Weight(:).*handles.y_Node(:))/sum(handles.Node_Weight);
Distance_To_CG=zeros(1,length(handles.Node));
for i=1:length(handles.Node)
    Distance_To_CG(i)=sqrt((handles.x_Node(i)-x_center_of_gravity)^2+(handles.y_Node(i)-y_center_of_gravity)^2);
end
Max_Distance_To_CG=max(Distance_To_CG);
Max_W=max(handles.Node_Weight);
%Tinh toan Merit
Merit=zeros(1,length(handles.Node));
for i=1:length(handles.Node)
    Merit(i)=0.5*(Max_Distance_To_CG-Distance_To_CG(i))/Max_Distance_To_CG +0.5*(handles.Node_Weight(i)/Max_W);
end
Merit(Condition1)=0;
Access_Node=[];
while find(Merit~=0)~=0
    [Merit_index index]=max(Merit);
    Merit(index)=0;
    handles.Backbone_Node =[handles.Backbone_Node index];    %Them nut Backbone moi vao tap hop cac nut Backbone
    delete(plot(index));
    draw(index)= scatter(handles.x_Node(index),handles.y_Node(index),'r','filled');
    for i=1:length(handles.Node)
        if (Merit(i)~=0)&(0.4*(sqrt((handles.x_Node(i)-handles.x_Node(index))^2+(handles.y_Node(i)-handles.y_Node(index))^2))<=Radius)
            Access_Node=[Access_Node i];    %Them nut truy nhap vao tap hop cac nut truy nhap tuong ung voi nut backbone
        end
    end
    for j=1:numel(Access_Node)
        h(j)=plot([handles.x_Node(index),handles.x_Node(Access_Node(j))],[handles.y_Node(index),handles.y_Node(Access_Node(j))],'g');   %Ve lien ket tu nut backbone den cac nut truy nhap
        node_index(Access_Node(j))=index;
    end
    Merit(Access_Node)=0;Merit_index=[];
    index=[];Access_Node=[];
end
set(handles.BB_Node,'String',num2str([handles.Backbone_Node]'));
%--------------------------------------------------------------------------
%Xay dung cay ket noi giua cac nut backbone voi nhau
Alpha=str2double(handles.ipt7);     %Nhap alpha
Backbone_Node_Weight = [];
for i=1:numel(handles.Backbone_Node)
    Backbone_Node_Weight_i = handles.Node_Weight(handles.Backbone_Node(i));
    Backbone_Node_Weight = [Backbone_Node_Weight Backbone_Node_Weight_i];
end
Node_Weight_a = [];
for i=1:numel(handles.Backbone_Node)
    Node_Weight_a_i = Backbone_Node_Weight;
    Node_Weight_a = [Node_Weight_a; Node_Weight_a_i];
end
Node_Weight_a = Node_Weight_a'
Cost_Backbone = [];
for i=1:numel(handles.Backbone_Node)
    for j = 1:numel(handles.Backbone_Node)
        Cost_Backbone(i,j) = Cost(handles.Backbone_Node(i),handles.Backbone_Node(j));
    end
end
Cost_Backbone
 moment=sum(Cost_Backbone(:,:).*Node_Weight_a(:,:))
[moment_Center_Backbone_Node,k]=min(moment);
handles.Center_Backbone_Node=handles.Backbone_Node(find(moment == moment_Center_Backbone_Node));delete(plot(handles.Center_Backbone_Node));
draw(handles.Center_Backbone_Node)=scatter(handles.x_Node(handles.Center_Backbone_Node),handles.y_Node(handles.Center_Backbone_Node),'b','filled');
set(handles.CBB_Node,'String',num2str([handles.Center_Backbone_Node]'));
%+------------------------------------------------------------------------+
%|                                Cau 2                                   |
%+------------------------------------------------------------------------+
%Dua tren thuat toan Prim-Dijkstra
Label=inf(1,length(handles.Node));  %Khoi tao nhan (Label)
Sub_Label=inf(1,length(handles.Node));
Label(handles.Center_Backbone_Node)=0;
Sub_Label(handles.Center_Backbone_Node)=0;
Connected_Backbone_Node=handles.Center_Backbone_Node;   
Condition2=handles.Center_Backbone_Node;
Predecessor=handles.Center_Backbone_Node;   %Khoi tao nut tien nhiem (Predecessor)
hop=zeros(1,numel(handles.Backbone_Node));  %Khoi tao buoc nhay (hop)
while numel(Connected_Backbone_Node)~=numel(handles.Backbone_Node)
    for i=1:numel(handles.Backbone_Node)
        if handles.Backbone_Node(i)~=Condition2
            Sub_Label(handles.Backbone_Node(i))=Alpha*Cost(handles.Backbone_Node(i),Predecessor)+Sub_Label(Predecessor);
            Label(handles.Backbone_Node(i))=Sub_Label(handles.Backbone_Node(i));
            hop(i)=hop(find(handles.Backbone_Node==Predecessor))+1;
        end
    end
    next_backbone_node=find(Sub_Label==min(Sub_Label(Sub_Label~=0)));
    Sub_Label(next_backbone_node)=0;
    Condition2=[Condition2,next_backbone_node];
    Connected_Backbone_Node=[Connected_Backbone_Node,next_backbone_node];
    Draw_Backbone_Tree(Predecessor)=plot([handles.x_Node(Predecessor),handles.x_Node(next_backbone_node)],[handles.y_Node(Predecessor),handles.y_Node(next_backbone_node)],'r');
    for j=1:numel(handles.Backbone_Node)
        if (handles.Backbone_Node(j)~=Condition2)&(Cost(handles.Backbone_Node(j),next_backbone_node)<Sub_Label(handles.Backbone_Node(j)))
            Predecessor=next_backbone_node;
            Sub_Label(handles.Backbone_Node(j))=Sub_Label(handles.Backbone_Node(j))+Sub_Label(Predecessor);
            Label(handles.Backbone_Node(j))=Label(handles.Backbone_Node(j))+Label(Predecessor);
            %Draw_Backbone_Tree(handles.Backbone_Node(j))=plot([handles.x_Node(Predecessor),handles.x_Node(handles.Backbone_Node(j))],[handles.y_Node(Predecessor),handles.y_Node(handles.Backbone_Node(j))],'r');
            hop(j)=hop(find(handles.Backbone_Node==Predecessor))+1;
        end
    end
end
for i=1:numel(handles.Backbone_Node)
    for j=1:numel(handles.Backbone_Node)
        sum_hop(j,i)=hop(j)+hop(i);
    end
end
umin=str2double(handles.ipt8);
for i=1:numel(handles.Backbone_Node)
    node_index(handles.Backbone_Node(i))=handles.Backbone_Node(i);
end
assignin('base','Node_Index',node_index);
for i=1:length(handles.Node)
    for j=1:length(handles.Node)
        if node_index(j)==node_index(i)
            handles.Traffic(j,i)=0;
            handles.Traffic(i,j)=0;
        elseif i==j
            handles.Traffic(j,i)=0;
            handles.Traffic(i,j)=0;
        else
            handles.Traffic(node_index(j),node_index(i))=handles.Traffic(node_index(j),node_index(i))+handles.Traffic(j,i);
%             handles.Traffic(node_index(i),node_index(j))=handles.Traffic(node_index(j),node_index(i));
            handles.Traffic(j,i)=0;
%             handles.Traffic(i,j)=handles.Traffic(j,i);
        end
    end
end
assignin('base','Traffic_2_after',handles.Traffic);
handles.home=zeros(numel(handles.Backbone_Node));
for i=1:numel(handles.Backbone_Node)
   for j=1:numel(handles.Backbone_Node)
       if sum_hop(j,i)==2
           handles.home(j,i)=handles.Center_Backbone_Node;
       elseif sum_hop(j,i)>2
           if Cost(handles.Backbone_Node(i),handles.Center_Backbone_Node)+Cost(handles.Center_Backbone_Node,handles.Backbone_Node(j))<Cost(handles.Backbone_Node(i),Predecessor)+Cost(Predecessor,handles.Backbone_Node(j))
                handles.home(j,i)=handles.Center_Backbone_Node;
           else handles.home(j,i)=Predecessor;
           end
       end
   end
end
n=zeros(numel(handles.Backbone_Node));
Utilization=zeros(numel(handles.Backbone_Node));
connection=zeros(numel(handles.Backbone_Node));
for i=1:numel(handles.Backbone_Node)
    for j=1:numel(handles.Backbone_Node)
        %if i~=handles.Center_Backbone_Node&j~=handles.Center_Backbone_Node&handles.Backbone_Node(i)~=handles.home&handles.Backbone_Node(j)~=handles.home
        n(j,i)=floor(handles.Traffic(handles.Backbone_Node(j),handles.Backbone_Node(i))/handles.Capacity);
        Utilization(j,i)=round((handles.Traffic(handles.Backbone_Node(j),handles.Backbone_Node(i))/(ceil(handles.Traffic(handles.Backbone_Node(j),handles.Backbone_Node(i))/handles.Capacity)*handles.Capacity)),3);
            if Utilization(j,i)>umin
                connection(j,i)=n(j,i)+1;
                connection(i,j)=connection(j,i);
            elseif Utilization(j,i)>0 & Utilization(j,i)<umin
                connection(j,i) = 1;
            else
                handles.Traffic(j,find(handles.Backbone_Node==handles.home(j,i)))=handles.Traffic(j,find(handles.Backbone_Node==handles.home(j,i)))+handles.Traffic(j,i);
                handles.Traffic(find(handles.Backbone_Node==handles.home(j,i)),i)=handles.Traffic(find(handles.Backbone_Node==handles.home(j,i)),i)+handles.Traffic(j,i);
                handles.Traffic(i,find(handles.Backbone_Node==handles.home(i,j)))=handles.Traffic(i,find(handles.Backbone_Node==handles.home(i,j)))+handles.Traffic(i,j);
                handles.Traffic(find(handles.Backbone_Node==handles.home(i,j)),j)=handles.Traffic(find(handles.Backbone_Node==handles.home(i,j)),j)+handles.Traffic(i,j);
            end
        %end
    end
end
connection
Utilization
Home_2 = handles.home
set(handles.Connect2,'data',connection);
set(handles.Utili2,'data',Utilization);
set(handles.Home2,'data',handles.home);
% +------------------------------------------------------------------------+
% |                                Cau 3                                   |
% +------------------------------------------------------------------------+
handles.Traffic_1=zeros(length(handles.Node));
for i=1:length(handles.Node)-3
    handles.Traffic_1(i,i+3)=1;
    handles.Traffic_1(i+3,i)=1;
end
for i=1:length(handles.Node)-6
    handles.Traffic_1(i,i+6)=2;
    handles.Traffic_1(i+6,i)=2;
end
for i=1:length(handles.Node)-7
    handles.Traffic_1(i,i+7)=3;
    handles.Traffic_1(i+7,i)=3;
end
% handles.Traffic_1(3,10)= handles.Traffic_1(3,10)+str2double(handles.ipt9);
% handles.Traffic_1(10,3)=handles.Traffic_1(3,10);
% handles.Traffic_1(15,30)= handles.Traffic_1(15,30)+str2double(handles.ipt10);
% handles.Traffic_1(30,15)=handles.Traffic_1(15,30);
% handles.Traffic_1(40,68)= handles.Traffic_1(40,68)+str2double(handles.ipt11);
% handles.Traffic_1(68,40)=handles.Traffic_1(40,68);
assignin('base','Traffic_3_before',handles.Traffic_1);
for i=1:length(handles.Node)
    for j=1:length(handles.Node)
        if node_index(j)==node_index(i)
            handles.Traffic_1(j,i)=0;
            handles.Traffic_1(i,j)=0;
        elseif i==j
            handles.Traffic_1(j,i)=0;
            handles.Traffic_1(i,j)=0;
        else
            handles.Traffic_1(node_index(j),node_index(i))=handles.Traffic_1(node_index(j),node_index(i))+handles.Traffic_1(j,i);
%             handles.Traffic_1(node_index(i),node_index(j))=handles.Traffic_1(node_index(j),node_index(i));
            handles.Traffic_1(j,i)=0;
%             handles.Traffic_1(i,j)=handles.Traffic_1(j,i);
        end
    end
end
handles.Traffic_1(node_index(3),node_index(10)) = handles.Traffic_1(node_index(3),node_index(10)) + str2double(handles.ipt9);
handles.Traffic_1(node_index(10),node_index(3)) = handles.Traffic_1(node_index(3),node_index(10));
handles.Traffic_1(node_index(15),node_index(30)) = handles.Traffic_1(node_index(15),node_index(30)) + str2double(handles.ipt10);
handles.Traffic_1(node_index(30),node_index(15)) = handles.Traffic_1(node_index(15),node_index(30));
handles.Traffic_1(node_index(40),node_index(68)) = handles.Traffic_1(node_index(40),node_index(68)) + str2double(handles.ipt11);
handles.Traffic_1(node_index(68),node_index(40)) = handles.Traffic_1(node_index(40),node_index(68));
for i=1:length(handles.Node)
    for j=1:length(handles.Node)
        if i==j
            handles.Traffic_1(i,j) = 0;
            handles.Traffic_1(j,i) = 0;
        end
    end
end
assignin('base','Traffic_3_after',handles.Traffic_1);
n=zeros(numel(handles.Backbone_Node));Utilization=zeros(numel(handles.Backbone_Node));
connection=zeros(numel(handles.Backbone_Node));
for i=1:numel(handles.Backbone_Node)
    for j=1:numel(handles.Backbone_Node)
        %if i~=handles.Center_Backbone_Node&j~=handles.Center_Backbone_Node&handles.Backbone_Node(i)~=handles.home&handles.Backbone_Node(j)~=handles.home
        n(j,i)=floor(handles.Traffic_1(handles.Backbone_Node(j),handles.Backbone_Node(i))/handles.Capacity);
        Utilization(j,i)=round((handles.Traffic_1(handles.Backbone_Node(j),handles.Backbone_Node(i))/(ceil(handles.Traffic_1(handles.Backbone_Node(j),handles.Backbone_Node(i))/handles.Capacity)*handles.Capacity)),3);
            if Utilization(j,i)>umin
                connection(j,i)=n(j,i)+1;
                connection(i,j)=connection(j,i);
            elseif Utilization(j,i)>0 & Utilization(j,i)<umin
                connection(j,i) = 1;
            else
                handles.Traffic_1(j,find(handles.Backbone_Node==handles.home(j,i)))=handles.Traffic_1(j,find(handles.Backbone_Node==handles.home(j,i)))+handles.Traffic_1(j,i);
                handles.Traffic_1(find(handles.Backbone_Node==handles.home(j,i)),i)=handles.Traffic_1(find(handles.Backbone_Node==handles.home(j,i)),i)+handles.Traffic_1(j,i);
                handles.Traffic_1(i,find(handles.Backbone_Node==handles.home(i,j)))=handles.Traffic_1(i,find(handles.Backbone_Node==handles.home(i,j)))+handles.Traffic_1(i,j);
                handles.Traffic_1(find(handles.Backbone_Node==handles.home(i,j)),j)=handles.Traffic_1(find(handles.Backbone_Node==handles.home(i,j)),j)+handles.Traffic_1(i,j);
            end
        %end
    end
end

connection_3=connection
Utilization_3=Utilization
Home_3 = handles.home
set(handles.Connect3,'data',connection);
set(handles.Utili3,'data',Utilization);
set(handles.Home3,'data',handles.home);

